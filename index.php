<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Testing</title>
</head>
<body>
    <form action="search.php" method="GET">
        <input type="text" size="60" name="query" placeholder="Введите запрос...">
        <input type="submit" value="Найти">
    </form>

    <style>

.pop_up_container{
        position: relative;
        width: 700px;
        border-radius: 5px;
        border: 1px solid black;
        margin: 0 auto 50px auto;
        padding: 0 20px 20px 20px;
        height: auto;
        max-height: 120px;
        overflow: hidden;
        cursor: pointer;
    }

    .pop_up_container.active{
        max-height: none;
    }

    .title{
        width: 100%;
        height: 130px;
    }
</style>

    <hr>
        Результат поиска по запросу: <b><?php echo $query ?></b>
        <?php if (mb_strlen($query) > 0): ?>
            <?php foreach ($arr as $values): ?>
            <div class="pop_up_container">
                <div class="title">
                    <p><b>Название: </b> <?php echo $values->snippet->title ?></p>
                    <p><b>Автор: </b><?php echo $values->snippet->channelTitle ?></p>
                    <p><b>Дата публикации: </b><?php echo date('d M. Y г.', strtotime($values->snippet->publishedAt)) ?></p>
                </div>
                <div class="video">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $values->id ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
            <hr>
        <?php endforeach ?>
            <?php else: echo 'Запрос пуст!' ?>
        <?php endif ?>
        <script type="text/javascript">
            let popUps = document.querySelectorAll('.pop_up_container');
            popUps.forEach(item => {
                item.addEventListener('click', function(){
                    if(item.classList.contains('active')){
                        item.classList.remove('active');
                    }
                    else{
                        item.classList.add('active');
                    }
                })
            });
        </script>
</body>
</html>