<?php

require_once 'vendor\autoload.php';

$KEY = 'AIzaSyBw2uBSA5ljdziXwbhfvk6lX303CasBgP0';

// Проверка ключа
$client = new Google_Client();
$client->setDeveloperKey($KEY);

// Отправка запросов на сервис
$youtube = new Google_Service_YouTube($client);

$query = $_GET['query'];

$searchResponse = $youtube->search->listSearch('snippet', array(
    'q' => $query,
    'type' => 'video',
    'order' => 'date',
    'maxResults' => 20,
));


foreach($searchResponse['items'] as $val) {
    $orderVideo[] = $val->id->videoId;
}


$searchVideos = $youtube->videos->listvideos('statistics, snippet', array(
    'id' => implode(',', $orderVideo),
));

$arr = $searchVideos['items'];

usort($arr, function($valueOne, $valueTwo) {
        return intval($valueTwo->statistics->viewCount) <=> intval($valueOne->statistics->viewCount);
});

// echo "<pre>";
// var_dump($arr);

require_once 'index.php';